package net.eldiosantos.test.mockserver.core.server;

import com.google.common.truth.Truth;
import net.eldiosantos.test.mockserver.core.server.api.PathPattern;
import net.eldiosantos.test.mockserver.core.server.api.RequestHandler;
import net.eldiosantos.test.mockserver.core.server.beans.Exchange;
import net.eldiosantos.test.mockserver.core.server.beans.ResponseStatus;
import net.eldiosantos.test.mockserver.core.server.handler.DefaultJettyRequestHandler;
import net.eldiosantos.test.mockserver.core.server.handler.RequestHandlerExecutor;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Created by esjunior on 30/03/2017.
 */
public class ServerManagerOkTest {

    public static final String TEST_REQUEST_ANSWER_03 = "{\"status\": \"it'a ok here. Roger that.\"}";
    public static final String PING_TEST_PATH = "/ping/test/path";
    public static final int SERVER_INSTANCE_PORT = 7171;
    public static final String CONTENT_TYPE_JSON = "application/json";
    public static final String CONTENT_TYPE_NAME = "Content-Type";
    private ServerManager srv = null;

    @Before
    public void setUp() throws Exception {
        final Map<PathPattern, RequestHandler>responses = new HashMap<>();

        responses.put(
            exchange -> PING_TEST_PATH.equals(exchange.getPath()) && exchange.getMethod().equals(Exchange.RequestMethod.GET)
            , exchange -> {
                    exchange.getOut().setBody(TEST_REQUEST_ANSWER_03);
                    exchange.getOut().getHeaders().put(CONTENT_TYPE_NAME, CONTENT_TYPE_JSON);
                    exchange.setStatus(ResponseStatus.OK);
                }
        );

        final DefaultJettyRequestHandler handler = new DefaultJettyRequestHandler(new RequestHandlerExecutor(responses));
        srv = new ServerManager(SERVER_INSTANCE_PORT, handler);
        srv.start();
    }

    @After
    public void tearDown() throws Exception {
        srv.stop();
    }

    @Test
    public void start() throws Exception {
        String responseBody = null;
        String testHeaderValue = null;
        String contentType = null;
        int statusCode = 0;

        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet httpGet = new HttpGet(String.format("http://localhost:%d%s", SERVER_INSTANCE_PORT, PING_TEST_PATH));
            try (CloseableHttpResponse response = httpclient.execute(httpGet)) {
                System.out.println(response.getStatusLine());
                final HttpEntity entity = response.getEntity();

                try(final BufferedReader reader = new BufferedReader(new InputStreamReader(entity.getContent(), Charset.forName("utf-8")))) {
                    responseBody = reader.lines().collect(Collectors.joining("\n"));
                    statusCode = response.getStatusLine().getStatusCode();
                    contentType = response.getFirstHeader(CONTENT_TYPE_NAME).getValue();

                } catch (Exception e) {
                    e.printStackTrace();
                    throw e;
                }
            }

        }

        // VALIDATE RESPONSE CODE
        System.out.println(String.format("[TEST_RESULT] statusCode: %d", statusCode));
        Truth.assertWithMessage("Let me see... The response code is ok?")
                .that(statusCode)
                .isEqualTo(200);

        // VALIDATE RESPONSE BODY
        System.out.println(String.format("[TEST_RESULT] responseBody: %s", responseBody));
        Truth.assertWithMessage("Let me see... The response body is right?")
                .that(responseBody)
                .isEqualTo(TEST_REQUEST_ANSWER_03);

        // VALIDATE RESPONSE CONTENT TYPE
        System.out.println(String.format("[TEST_RESULT] contentType: %s", contentType));
        Truth.assertWithMessage("Let me see... The response code is ok?")
                .that(contentType)
                .isEqualTo(CONTENT_TYPE_JSON);

    }
}
