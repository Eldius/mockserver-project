package net.eldiosantos.test.mockserver.core.server.handler.helper;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by esjunior on 31/03/2017.
 */
public class HeadersHelper {

    private static final Logger LOG = Log.getLogger(HeadersHelper.class);

    public Map<String, String> extractRequestHeaders(final HttpServletRequest request) {
        return Collections.list(request.getHeaderNames())
                .parallelStream()
                .collect(Collectors.toMap(name -> name, request::getHeader));
    }

    public void setResponseHeaders(final Map<String, String> headersMap, final HttpServletResponse response) {
        headersMap
            .entrySet()
            .forEach(entry -> {
                if(LOG.isDebugEnabled()) {
                    LOG.debug("adding header: %s => %s", entry.getKey(), entry.getValue());
                }

                response.addHeader(entry.getKey(), entry.getValue());
            });
    }
}
