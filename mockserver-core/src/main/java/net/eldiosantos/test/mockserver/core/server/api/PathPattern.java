package net.eldiosantos.test.mockserver.core.server.api;

import net.eldiosantos.test.mockserver.core.server.beans.Exchange;

/**
 * Verify if the RequestHandler can execute
 * this request based on it's path.
 */
public interface PathPattern {
    Boolean validate(Exchange exchange);
}
