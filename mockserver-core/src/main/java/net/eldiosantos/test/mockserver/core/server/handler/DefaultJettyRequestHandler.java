package net.eldiosantos.test.mockserver.core.server.handler;

import net.eldiosantos.test.mockserver.core.server.api.PathPattern;
import net.eldiosantos.test.mockserver.core.server.api.RequestHandler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

/**
 * Created by esjunior on 30/03/2017.
 */
public class DefaultJettyRequestHandler extends AbstractHandler {

    private static final Logger LOG = Log.getLogger(DefaultJettyRequestHandler.class);

    private final RequestHandlerExecutor mapping;

    public DefaultJettyRequestHandler() {
        mapping = new RequestHandlerExecutor();
    }

    public DefaultJettyRequestHandler(RequestHandlerExecutor mapping) {
        this.mapping = mapping;
    }

    @Override
    public void handle(String target, Request baseRequest, HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        LOG.info("Requesting '{}' with method '{}'", target, request.getMethod());
        mapping.execute(target, baseRequest, request, response);
        LOG.info("Responding '{}' with method '{}' and headers '{}'", target, request.getMethod(), response.getHeaderNames());
    }

    public RequestHandler put(PathPattern key, RequestHandler value) {
        return mapping.put(key, value);
    }

    public void putAll(Map<? extends PathPattern, ? extends RequestHandler> m) {
        mapping.putAll(m);
    }

    public RequestHandler putIfAbsent(PathPattern key, RequestHandler value) {
        return mapping.putIfAbsent(key, value);
    }

    public boolean remove(Object key, Object value) {
        return mapping.remove(key, value);
    }
}
