package net.eldiosantos.test.mockserver.core;

import net.eldiosantos.test.mockserver.core.server.ServerManager;
import net.eldiosantos.test.mockserver.core.server.api.PathPattern;
import net.eldiosantos.test.mockserver.core.server.api.RequestHandler;
import net.eldiosantos.test.mockserver.core.server.handler.DefaultJettyRequestHandler;
import net.eldiosantos.test.mockserver.core.server.handler.RequestHandlerExecutor;
import org.eclipse.jetty.server.Handler;
import org.junit.Test;

import java.lang.reflect.Method;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by esjunior on 04/04/2017.
 */
public class ServerManagerTestHelper {

    private static final Map<String, ServerManager>managersMapping = new HashMap<>();
    private static final Map<String, Handler>handlersMapping = new HashMap<>();
    private static final SecureRandom random = new SecureRandom();

    public ServerManager create(final Handler handler, final Boolean autoStart) throws Exception {
        ServerManager sm = new ServerManager(getUniquePort(), handler);

        final String testMethodName = getTestMethod();
        managersMapping.put(testMethodName, sm);
        handlersMapping.put(testMethodName, handler);

        if(autoStart != null && autoStart) {
            sm.start();
        }

        return sm;
    }

    public ServerManager create(final Map<PathPattern, RequestHandler> mappings, final Boolean autoStart) throws Exception {
        final RequestHandlerExecutor executor = new RequestHandlerExecutor(mappings);
        final DefaultJettyRequestHandler jettyHandler = new DefaultJettyRequestHandler(executor);
        return create(jettyHandler, autoStart);
    }

    public ServerManager create(final Map<PathPattern, RequestHandler> mappings) throws Exception {
        return create(mappings, true);
    }

    public ServerManager create() throws Exception {
        return create(new HashMap<PathPattern, RequestHandler>(), true);
    }

    private ServerManagerTestHelper addMapping(final PathPattern pattern, final RequestHandler handler) {
        try {
            final DefaultJettyRequestHandler jettyHandler = (DefaultJettyRequestHandler)handlersMapping.get(getTestMethod());
            jettyHandler.put(pattern, handler);
            return this;
        }catch (ClassCastException e) {
            throw new IllegalStateException("The handler type isan't compatible with dinamic handlers management", e);
        }
    }

    public ServerManager get() {
        return managersMapping.get(getTestMethod());
    }

    private String getTestMethod() {
        final StackTraceElement stel = Arrays.stream(Thread.getAllStackTraces().get(Thread.currentThread()))
            .filter(st -> {
                Boolean found = false;
                try {
                    System.out.printf("method: %s.%s%n", st.getClassName(), st.getMethodName());
                    final Class<?> clazz = Class.forName(st.getClassName());
                    final Method method = clazz.getDeclaredMethod(st.getMethodName());
                    found = method.isAnnotationPresent(Test.class);
                } catch (Exception e) {
                    //throw new IllegalStateException("Error trying to find class and method", e);
                }
                return found;
            }).findFirst().get();

        return new StringBuffer(stel.getClassName()).append(".").append(stel.getMethodName()).toString();
    }

    private Integer getUniquePort() {
        Integer port = getRandomPort();
        while (verifyPortInUse(port)){
            port = getRandomPort();
        }

        return port;
    }

    private Boolean verifyPortInUse(final Integer port) {
        return managersMapping.entrySet()
                .parallelStream()
                .anyMatch(e -> e.getValue().getPort() == port);
    }

    private Integer getRandomPort() {
        return random.nextInt(1024) + 1024;
    }

    public Collection<ServerManager>getManagers() {
        return managersMapping.values();
    }
}
