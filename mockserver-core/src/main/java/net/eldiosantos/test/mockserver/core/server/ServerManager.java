package net.eldiosantos.test.mockserver.core.server;

import net.eldiosantos.test.mockserver.core.server.handler.DefaultJettyRequestHandler;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import java.security.SecureRandom;

/**
 * Start and stop server instances.
 * Each ServerManager represents a server instance.
 */
public class ServerManager {

    private static final Logger LOG = Log.getLogger(ServerManager.class);

    private final Server server;
    private final Handler handler;

    private final Integer port;

    /**
     * If you don't pass a port number it will use a random number.
     */
    public ServerManager() {
        this.port = new SecureRandom().nextInt(1024) + 1024;
        server = new Server(this.port);
        handler = new DefaultJettyRequestHandler();
    }

    public ServerManager(final int port, Handler handler) {
        this.port = port;
        server = new Server(this.port);
        this.handler = handler;
    }

    public Server start() throws Exception {
        server.setHandler(handler);
        server.start();
        server.dumpStdErr();
        //server.join();
        while((!server.isRunning()) && (!server.isStarted()) && (server.isStarting())) {
            LOG.info("Still starting server...");
            Thread.sleep(500L);
        }
        LOG.info("Server started...");
        return server;
    }

    protected Handler getHandler() {
        return this.handler;
    }

    public void stop() throws Exception {
        server.stop();
    }

    public Integer getPort() {
        return this.port;
    }

    public Boolean isActive() {
        return this.isStopped();
    }

    public Boolean isRunning() {
        return server.isRunning();
    }

    public Boolean isStarted() {
        return server.isStarted();
    }

    public Boolean isStarting() {
        return server.isStarting();
    }

    public Boolean isStopping() {
        return server.isStopping();
    }

    public Boolean isStopped() {
        return server.isStopped();
    }

    public Boolean isFailed() {
        return server.isFailed();
    }
}
