package net.eldiosantos.test.mockserver.core.server.handler;

import net.eldiosantos.test.mockserver.core.server.api.PathPattern;
import net.eldiosantos.test.mockserver.core.server.api.RequestHandler;
import net.eldiosantos.test.mockserver.core.server.api.impl.EmptyRequestHandler;
import net.eldiosantos.test.mockserver.core.server.beans.Exchange;

import java.util.Map;
import java.util.Optional;

/**
 * Resolve the {@link RequestHandler} to be used to handle
 * the request.
 */
public class RequestHandlerResolver {

    private final Map<PathPattern, RequestHandler> map;

    private final RequestHandler defaultHandler = new EmptyRequestHandler();

    public RequestHandlerResolver(Map<PathPattern, RequestHandler> map) {
        this.map = map;
    }

    public RequestHandler resolve(final Exchange exchange) {
        final Optional<Map.Entry<PathPattern, RequestHandler>> optEntry = map.entrySet()
                .parallelStream()
                .filter(e -> e.getKey().validate(exchange))
                .findAny();

        if(optEntry.isPresent()) {
            return optEntry.get().getValue();
        } else {
            return this.defaultHandler;
        }
    }

    public RequestHandler put(PathPattern key, RequestHandler value) {
        return map.put(key, value);
    }

    public void putAll(Map<? extends PathPattern, ? extends RequestHandler> m) {
        map.putAll(m);
    }

    public RequestHandler putIfAbsent(PathPattern key, RequestHandler value) {
        return map.putIfAbsent(key, value);
    }

    public boolean remove(Object key, Object value) {
        return map.remove(key, value);
    }
}
