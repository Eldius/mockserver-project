package net.eldiosantos.test.mockserver.core.server.beans;

/**
 * Represent the response status with it's code and message.
 */
public class ResponseStatus {

    private Integer code;
    private String message;

    public ResponseStatus() {
    }

    public ResponseStatus(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public ResponseStatus setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseStatus setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public String toString() {
        return "ResponseStatus{" +
                "code=" + code +
                ", message='" + message + '\'' +
                '}';
    }

    public static ResponseStatus OK = new ResponseStatus(200, "IT'S OK!");
    public static ResponseStatus FORBIDDEN = new ResponseStatus(403, "YOU CAN'T REACH THAT!");
    public static ResponseStatus NOT_FOUND = new ResponseStatus(404, "IT DOESN'T EXISTS!");
    public static ResponseStatus OK_EMPTY = new ResponseStatus(204, "OK WITHOUT CONTENT");
    public static ResponseStatus I_AM_A_TEAPOT = new ResponseStatus(418, "I'M A TEAPOT");
}
