package net.eldiosantos.test.mockserver.core.server.beans;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * The request or response data itself.
 */
public class ExchangeData {

    /**
     * Request/Response header.
     */
    private final Map<String, String>headers;

    /**
     * Request/Response body.
     */
    private String body = "";

    public ExchangeData() {
        headers = new HashMap<>();
    }

    public ExchangeData(Map<String, String> headers) {
        this.headers = headers;
    }

    public ExchangeData(Map<String, String> headers, String body) {
        this.headers = headers;
        this.body = body;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public String getBody() {
        return body;
    }

    public Optional<String> getBodyAsOptional() {
        return body != null ? Optional.of(body) : Optional.empty();
    }

    public ExchangeData setBody(String body) {
        this.body = body;
        return this;
    }
}
