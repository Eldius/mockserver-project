package net.eldiosantos.test.mockserver.core.server.handler.helper;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.stream.Collectors;

/**
 * Help to work with body of requests and responses.
 */
public class BodyHelper {

    /**
     * Parse request body to {@link String}.
     * @param request received request object.
     * @return content of request body.
     */
    public String extractRequestBody(final HttpServletRequest request) {
        String body = null;
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(request.getInputStream(), "utf-8"))) {
            body = reader.lines().collect(Collectors.joining("\n"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return body;
    }

    public void setResponsebody(final String body, final HttpServletResponse response) {
        try {
            final PrintWriter pw = response.getWriter();
            pw.append(body);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
