package net.eldiosantos.test.mockserver.core.server.beans;

/**
 * The object to represent the data exchange in requests.
 */
public class Exchange {
    /**
     * The incoming request data.
     */
    private final ExchangeData in;

    /**
     * The outcome response data.
     */
    private final ExchangeData out;

    private String path;
    private RequestMethod method;

    /**
     * Represents the response status.
     */
    private ResponseStatus status = null;

    public Exchange(ExchangeData in, ExchangeData out, String path, RequestMethod method) {
        this.in = in;
        this.out = out;
        this.path = path;
        this.method = method;
    }

    public Exchange(ExchangeData in, ExchangeData out) {
        this.in = in;
        this.out = out;
    }

    public Exchange() {
        in = new ExchangeData();
        out = new ExchangeData();
    }

    public ExchangeData getIn() {
        return in;
    }

    public ExchangeData getOut() {
        return out;
    }

    public String getPath() {
        return path;
    }

    public Exchange setPath(String path) {
        this.path = path;
        return this;
    }

    public RequestMethod getMethod() {
        return method;
    }

    public Exchange setMethod(RequestMethod method) {
        this.method = method;
        return this;
    }

    /**
     * Verifies the response is about an error request.
     * @return error state
     */
    public Boolean isError() {
        return !this.isOk();
    }

    /**
     * Verifies the response is about a success request.
     * @return response state
     */
    public Boolean isOk() {
        return (this.getStatus().getCode() / 100) == 2;
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public Exchange setStatus(ResponseStatus status) {
        if(status == null) {
            throw new IllegalArgumentException("Response status could not be null");
        }
        this.status = status;
        return this;
    }

    public enum RequestMethod {
        GET
        , HEAD
        , POST
        , PUT
        , DELETE
        , CONNECT
        , OPTIONS
        , TRACE
        , PATCH;
    }
}

