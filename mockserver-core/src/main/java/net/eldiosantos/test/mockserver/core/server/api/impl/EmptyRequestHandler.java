package net.eldiosantos.test.mockserver.core.server.api.impl;

import net.eldiosantos.test.mockserver.core.server.api.RequestHandler;
import net.eldiosantos.test.mockserver.core.server.beans.Exchange;

/**
 * Created by esjunior on 31/03/2017.
 */
public class EmptyRequestHandler implements RequestHandler {
    @Override
    public void handle(Exchange exchange) {
        exchange.getOut().setBody("");
        exchange.getOut().getHeaders().putAll(exchange.getIn().getHeaders());
    }
}
