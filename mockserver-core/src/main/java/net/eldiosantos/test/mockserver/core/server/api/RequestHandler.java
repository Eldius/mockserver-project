package net.eldiosantos.test.mockserver.core.server.api;

import net.eldiosantos.test.mockserver.core.server.beans.Exchange;

/**
 * Handles the request.
 */
public interface RequestHandler {
    void handle(Exchange exchange);
}
