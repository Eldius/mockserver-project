package net.eldiosantos.test.mockserver.core.server.handler;

import net.eldiosantos.test.mockserver.core.server.api.PathPattern;
import net.eldiosantos.test.mockserver.core.server.api.RequestHandler;
import net.eldiosantos.test.mockserver.core.server.beans.Exchange;
import net.eldiosantos.test.mockserver.core.server.handler.helper.BodyHelper;
import net.eldiosantos.test.mockserver.core.server.handler.helper.HeadersHelper;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by esjunior on 30/03/2017.
 */
public class RequestHandlerExecutor {

    private static final Logger LOG = Log.getLogger(RequestHandlerExecutor.class);

    private final HeadersHelper headersHelper = new HeadersHelper();
    private final BodyHelper bodyHelper = new BodyHelper();
    private final RequestHandlerResolver resolver;

    public RequestHandlerExecutor(Map<PathPattern, RequestHandler> map) {
        this.resolver = new RequestHandlerResolver(map);
    }

    public RequestHandlerExecutor() {
        this(new HashMap<>());
    }

    public void execute(
            final String target,
            final Request baseRequest,
            final HttpServletRequest request,
            final HttpServletResponse response
    ) {
        final Exchange exchange = new Exchange();

        exchange.setMethod(Exchange.RequestMethod.valueOf(request.getMethod())).setPath(target);

        // HANDLE REQUEST BODY
        exchange.getIn().setBody(bodyHelper.extractRequestBody(request));

        // HANDLE REQUEST HEADERS
        exchange.getIn().getHeaders().putAll(headersHelper.extractRequestHeaders(request));

        // HANDLE RESPONSE BODY
        // RESOLVE THE REQUEST TO BE USED
        // PROCESS THE REQUEST
        resolver.resolve(exchange).handle(exchange);

        LOG.info(String.format("outHeaders: %s", exchange.getOut().getHeaders()));

        // HANDLE RESPONSE HEADERS
        headersHelper.setResponseHeaders(exchange.getOut().getHeaders(), response);


        if(exchange.isError()) {
            try {
                response.sendError(exchange.getStatus().getCode(), exchange.getStatus().getMessage());
            } catch (IOException e) {
                LOG.warn("Error setting up the error message (I know, it's a little strange to read here, but it's true).", e);
                e.printStackTrace();
            }

        } else {
            response.setStatus(exchange.getStatus().getCode());
        }

        bodyHelper.setResponsebody(exchange.getOut().getBody(), response);

        baseRequest.setHandled(true);

        final StringBuffer msg = new StringBuffer("\n** RESPONSE *****************************************\n")
                .append("response code: ").append(response.getStatus()).append("\n")
                .append("headers: ").append(response.getHeaderNames()).append("\n")
                .append("content: ").append("\n");
        LOG.info(msg.toString());
    }

    public RequestHandler put(PathPattern key, RequestHandler value) {
        return resolver.put(key, value);
    }

    public void putAll(Map<? extends PathPattern, ? extends RequestHandler> m) {
        resolver.putAll(m);
    }

    public RequestHandler putIfAbsent(PathPattern key, RequestHandler value) {
        return resolver.putIfAbsent(key, value);
    }

    public boolean remove(Object key, Object value) {
        return resolver.remove(key, value);
    }
}
