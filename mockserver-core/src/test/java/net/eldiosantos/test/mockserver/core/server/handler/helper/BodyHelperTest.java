package net.eldiosantos.test.mockserver.core.server.handler.helper;

import com.google.common.truth.Truth;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;

/**
 * Created by Eldius on 31/03/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class BodyHelperTest {

    @Mock
    private HttpServletRequest request;

    @Mock
    private ServletInputStream in;

    @Mock
    private HttpServletResponse response;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void extractRequestBody() throws Exception {
        final BodyHelper helper = new BodyHelper();

        final String body = "{\"attrib1\": \"value1\"}";

        final ByteArrayInputStream byteArrayInputStream=new ByteArrayInputStream(body.getBytes("utf-8"));
        ServletInputStream servletInputStream=new ServletInputStream(){
            @Override
            public boolean isFinished() {
                return byteArrayInputStream.available() == 0;
            }

            @Override
            public boolean isReady() {
                return byteArrayInputStream.available() > 0;
            }

            @Override
            public void setReadListener(ReadListener readListener) {

            }

            public int read() throws IOException {
                return byteArrayInputStream.read();
            }
        };

        Mockito.when(request.getInputStream()).thenReturn(servletInputStream);
        Mockito.when(request.getHeader("Header-Name")).thenReturn("Test value");
        String requestBody = helper.extractRequestBody(request);

        Truth.assertWithMessage("Let me see if we received the right body content")
                .that(requestBody)
                .isEqualTo(body);
    }

    @Test
    public void setResponsebody() throws Exception {
        final ByteArrayOutputStream out = new ByteArrayOutputStream();

        // TODO FIND A WAY TO SET THE TEXT ENCODE BEFORE CONFIG FINDBUGS TO FAIL THE BUILD
        final PrintWriter pw = new PrintWriter(out);
        Mockito.when(response.getWriter())
                .thenReturn(pw);

        final String body = "{\"attrib1\": \"value1\"}";

        final BodyHelper helper = new BodyHelper();

        helper.setResponsebody(body, response);

        pw.flush();

        Truth.assertWithMessage("Let me see the response body")
                .that(out.toString("utf-8"))
                .isEqualTo(body);
    }
}
