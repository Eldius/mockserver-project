package net.eldiosantos.test.mockserver.core.server.beans;

import com.google.common.truth.Truth;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by esjunior on 04/04/2017.
 */
public class ExchangeTest {
    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void isResponseOk() throws Exception {
        final Exchange exchange = new Exchange();

        exchange.setStatus(ResponseStatus.OK);

        System.out.printf("STATUS: %s%n", exchange.getStatus().toString());
        Truth.assertWithMessage("Do we have an OK answer?")
                .that(exchange.isError())
                .isFalse();
    }

    @Test
    public void isResponseOkAgain() throws Exception {
        final Exchange exchange = new Exchange();

        exchange.setStatus(ResponseStatus.OK_EMPTY);

        System.out.printf("STATUS: %s%n", exchange.getStatus().toString());
        Truth.assertWithMessage("Do we have an OK answer?")
                .that(exchange.isError())
                .isFalse();
    }

    @Test
    public void isResponseInError() throws Exception {
        final Exchange exchange = new Exchange();

        exchange.setStatus(ResponseStatus.FORBIDDEN);

        System.out.printf("STATUS: %s%n", exchange.getStatus().toString());
        Truth.assertWithMessage("Do we have a 403 error answer?")
                .that(exchange.isError())
                .isTrue();
    }

    @Test
    public void isResponseInErrorAgain() throws Exception {
        final Exchange exchange = new Exchange();

        exchange.setStatus(ResponseStatus.NOT_FOUND);

        System.out.printf("STATUS: %s%n", exchange.getStatus().toString());
        Truth.assertWithMessage("Do we have a 404 error answer?")
                .that(exchange.isError())
                .isTrue();
    }

    @Test
    public void isResponseInErrorAgainAgain() throws Exception {
        final Exchange exchange = new Exchange();

        exchange.setStatus(ResponseStatus.I_AM_A_TEAPOT);

        System.out.printf("STATUS: %s%n", exchange.getStatus().toString());
        Truth.assertWithMessage("Do we have a 404 error answer?")
                .that(exchange.isError())
                .isTrue();
    }

}
