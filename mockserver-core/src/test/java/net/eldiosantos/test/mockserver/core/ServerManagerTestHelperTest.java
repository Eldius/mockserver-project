package net.eldiosantos.test.mockserver.core;

import com.google.common.truth.Truth;
import net.eldiosantos.test.mockserver.core.server.ServerManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by esjunior on 04/04/2017.
 */
public class ServerManagerTestHelperTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void create() throws Exception {
        final ServerManager sm = new ServerManagerTestHelper().create();
        final ServerManager sm2 = new ServerManagerTestHelper().get();

        Truth.assertWithMessage("We have the same server manager?")
                .that(sm)
                .isEqualTo(sm2);
    }

    @Test
    public void get() throws Exception {
        final ServerManager sm = new ServerManagerTestHelper().create();
        final ServerManager sm2 = new ServerManagerTestHelper().get();

        Truth.assertWithMessage("We have the same server manager?")
                .that(sm)
                .isEqualTo(sm2);
    }

    @Test
    public void get_without_create() throws Exception {
        final ServerManager sm = new ServerManagerTestHelper().get();

        Truth.assertWithMessage("We have the same server manager?")
                .that(sm)
                .isNull();
    }

}