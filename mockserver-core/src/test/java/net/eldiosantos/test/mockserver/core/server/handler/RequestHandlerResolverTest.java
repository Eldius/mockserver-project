package net.eldiosantos.test.mockserver.core.server.handler;

import com.google.common.truth.Truth;
import net.eldiosantos.test.mockserver.core.server.api.PathPattern;
import net.eldiosantos.test.mockserver.core.server.api.RequestHandler;
import net.eldiosantos.test.mockserver.core.server.api.impl.EmptyRequestHandler;
import net.eldiosantos.test.mockserver.core.server.beans.Exchange;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;

import static org.junit.Assert.*;

/**
 * Created by esjunior on 31/03/2017.
 */
public class RequestHandlerResolverTest {

    final EmptyRequestHandler defaultHandler = new EmptyRequestHandler();

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void resolve() throws Exception {

        final HashMap<PathPattern, RequestHandler> handlersMapping = new HashMap<>();
        handlersMapping.put(exchange -> true, defaultHandler);
        final RequestHandlerResolver resolver = new RequestHandlerResolver(handlersMapping);

        final Exchange exchange = new Exchange()
                .setPath("/my/fake/path")
                .setMethod(Exchange.RequestMethod.GET);

        final RequestHandler handler = resolver.resolve(exchange);

        Truth.assertWithMessage("The resolver returned the same handler?")
                .that(handler)
                .isEqualTo(defaultHandler);


    }

    @Test
    public void resolve_empty_map() throws Exception {
        final HashMap<PathPattern, RequestHandler> handlersMapping = new HashMap<>();

        final RequestHandlerResolver resolver = new RequestHandlerResolver(handlersMapping);

        final Exchange exchange = new Exchange()
                .setPath("/my/fake/path")
                .setMethod(Exchange.RequestMethod.GET);

        final RequestHandler handler = resolver.resolve(exchange);

        Truth.assertWithMessage("The resolver returned the same handler?")
                .that(handler.getClass())
                .isEqualTo(defaultHandler.getClass());

    }

    @Test
    public void resolve_default_multiple_handlers_map() throws Exception {

        final HashMap<PathPattern, RequestHandler> handlersMapping = new HashMap<>();
        final RequestHandler rightHandler = exchange -> exchange.getOut().setBody(exchange.getIn().getBody());

        handlersMapping.put(exchange -> exchange.getPath().equals("/my/fake/path"), rightHandler);
        handlersMapping.put(exchange -> exchange.getPath().equals("/my/fake/path1"), exchange -> exchange.getOut().setBody(exchange.getIn().getBody()));
        handlersMapping.put(exchange -> exchange.getPath().equals("/my/fake/path2"), exchange -> exchange.getOut().setBody(exchange.getIn().getBody()));
        handlersMapping.put(exchange -> exchange.getPath().equals("/my/fake/path3"), exchange -> exchange.getOut().setBody(exchange.getIn().getBody()));

        final RequestHandlerResolver resolver = new RequestHandlerResolver(handlersMapping);

        final Exchange exchange = new Exchange()
                .setPath("/my/fake/path")
                .setMethod(Exchange.RequestMethod.GET);

        final RequestHandler handler = resolver.resolve(exchange);

        Truth.assertWithMessage("The resolver returned the same handler?")
                .that(handler)
                .isEqualTo(rightHandler);

    }

}