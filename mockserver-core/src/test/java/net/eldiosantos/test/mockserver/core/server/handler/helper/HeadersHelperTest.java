package net.eldiosantos.test.mockserver.core.server.handler.helper;

import com.google.common.truth.Truth;
import net.eldiosantos.test.mockserver.core.server.beans.Exchange;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;

/**
 * Created by Eldius on 01/04/2017.
 */
@RunWith(MockitoJUnitRunner.class)
public class HeadersHelperTest {

    public static final String HEADER_NAME = "Header-Name";
    public static final String HEADER_VALUE = "Test value";
    @Mock
    private HttpServletRequest request;

    @Mock
    private HttpServletResponse response;

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void extractRequestHeaders() throws Exception {
        final HeadersHelper helper = new HeadersHelper();

        final List<String> names = new ArrayList<>();
        names.add(HEADER_NAME);
        Mockito.when(request.getHeaderNames()).thenReturn(Collections.enumeration(names));
        Mockito.when(request.getHeader(HEADER_NAME)).thenReturn(HEADER_VALUE);

        Map<String, String> headers = helper.extractRequestHeaders(request);

        Truth.assertWithMessage("How Do we have just one header?")
                .that(headers.size())
                .isEqualTo(1);
        Truth.assertWithMessage("What is the header value?")
                .that(headers.get(HEADER_NAME))
                .isEqualTo(HEADER_VALUE);
    }

    @Test
    public void setResponseHeaders() throws Exception {

        final Map<String, String>headers = new HashMap<>();

        headers.put("Content-Type", "unknown/text");
        headers.put("Fake-Header", "Fake value");

        final HeadersHelper helper = new HeadersHelper();

        helper.setResponseHeaders(headers, response);

        Mockito.verify(response, Mockito.atLeastOnce()).addHeader("Content-Type", "unknown/text");
        Mockito.verify(response, Mockito.atLeastOnce()).addHeader("Fake-Header", "Fake value");

    }

    @Test
    public void setResponseHeaders_empty() throws Exception {

        final Map<String, String>headers = new HashMap<>();

        final HeadersHelper helper = new HeadersHelper();

        helper.setResponseHeaders(headers, response);

        Mockito.verifyZeroInteractions(response);

    }

}
