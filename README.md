# MOCK SERVER PROJECT #

Here I'm trying to create a tool to test REST client implementations creating mock servers...

### What is this repository for? ###

* Quick summary:
    - mockserver-core: The server management project;
* Version:
    - 0.0.1-SNAPSHOT
* Build Status:

    [![CircleCI](https://circleci.com/bb/Eldius/mockserver-project.svg?style=svg)](https://circleci.com/bb/Eldius/mockserver-project)
